---
title: "Category archive pages and Jekyll"
category: Technology
tags: jekyll
excerpt: "My experience using jekyll-archives with minimal-mistakes"
header:
  overlay_image: /assets/images/teaser-macbook.jpg
  overlay_filter: 0.5
teaser: /assets/images/unsplash-macbook.jpg
date: 2020-06-11 02:00 
---
In today's experimentation, I did some work on the masthead and category archives.  originally, I had created pages for each category and placed them in the `_pages` folder and enabled them in `_data/navigation.yml` in the `main` array. Since i'm not using GitHub pages however, I have the freedom of a bit more plugins.  So I decided that rather than crowd the masthead with links to post categories, I would create a [custom sidebar](https://mmistakes.github.io/minimal-mistakes/docs/layouts/#custom-sidebar-navigation-menu) that would display on each post page **and** the archive pages.

## Using archives

The basics of using [jekyll-archives](https://github.com/jekyll/jekyll-archives) with minimal-mistakes can be found [here](https://mmistakes.github.io/minimal-mistakes/docs/configuration/#archive-settings). The gist is, Jekyll automatically creates an archive page for each category and tag used in their respective locations (`/tags/tag-name`, `/categories/category-name`, etc...). For now, I only have to categories I post in, General and Technology, but I plan on adding more as I go. 

Once the plugin is installed, it's time to configure Jekyll! I stuck with the instructions given by minimal-mistakes and my `_config.yml` looks like this:

```yml
# archive pages
category_archive:
  type: jekyll-archives
  path: /categories/
tag_archive:
  type: jekyll-archives
  path: /tags/
jekyll-archives:
  enabled:
    - categories
    - tags
  layouts:
    category: archive-taxonomy
    tag: archive-taxonomy
  permalinks:
    category: /categories/:name/
    tag: /tags/:name/
```

Once configured, I started up the Jekyll server and navigated to one of the generated pages to verify that everything worked correctly. Everything checks out! Next step: clean up the masthead and create the sidebar!

In the `_data/navigation.yml` file I removed the links to general and technology and created a new custom sidebar called `post`.Create a title for the sidebar and then add the url's for the corresponding categries. Since  only two categories seemed a bit bare for now, I also added some lines for tags below that. As I go I'll add frequently used ones so that they're easier to access then browsing through the [all posts]({% link _pages/all-posts.md %}) page. 

```yml
post:
  - title: Categories
    children:
      - title: General
        url: /categories/general/
      - title: Technology
        url: /categories/technology/

  - title: Tags
    children:
      - title: Jekyll
        url: /tags/jekyll/
      - title: Markdown
        url: /tags/markdown/
```

Next, add the custom sidebar to the desired pages. In my case that includes posts, category archives and tag archives. To save a future headache, adding the sidebar to your `defaults:` in `_config.yml` is the easiest.  I already have a section for posts so all it took was adding `sidebar: nav: "post"`, full configuration below.

```yml
defaults:
...
  # _posts
  - scope:
      path: ""
      type: posts
    values:
      sidebar:
        nav: post
      layout: single
      author_profile: true
      read_time: true
      comments: false
      share: false
      related: true
```

Finally, it's time to add the navbar to the archive pages. But wait! Those pages are generated during build-time and don't exist in my repo! Luckily this can also be configured with `defaults` in your config file as well! Add a new `scope:` and leave the `path:` blank and in `type:` insert either `category` or `tag`. In the `values:` add the sidebar nav you created and you're good to go! The configurations are below!

```yml
defaults:
...
  - scope:
      path: ""
      type: category
    values:
      sidebar:
        nav: post
  - scope:
      path: ""
      type: tag
    values:
      sidebar:
        nav: post
```

{% include figure image_path="/assets/images/screenshots/screenshot-general-archive.png" alt="general archive page" caption="The General category page with custom sidebar" %}
