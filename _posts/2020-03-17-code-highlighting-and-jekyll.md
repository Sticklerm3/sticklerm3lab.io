---
title: "Code highlighting and Jekyll"
description: "Customizing syntax with rouge"
category: Technology
tags: 
  - jekyll
  - code
  - syntax highlighting
date: "2020-03-17 07:20 -0500"
header:
  overlay_image: /assets/images/teaser-macbook.jpg
  teaser: /assets/images/overlay-macbook.jpg
excerpt: "Get inline syntax highlighting for code with a few short steps."
---

[Jekyll] is a super easy way to create static websites with tons of features! One of those features is syntax highlighting for code snippets. In this post we'll take a look at customizing your syntax highlights a bit.

  [Jekyll]: https://jekyllrb.com/ "Jekyll homepage"

## Changing your syntax style with rouge

If you don't like the syntax highlights provided by your theme or just want to change it up a bit, you can use the [Rouge] gem to create a new highlighting file.  Rouge is the default highlighter provided by Jekyll which makes it even easier to use! If you don't already, install the rouge gem with `gem install rouge`. To use the gem on the cli, the command is `rougify`. To list available styles type `rougify help style`. Next, we'll create your new stylesheet in the `assets/css/` folder with your chosen style. Alternatively, if you don't like any of the available styles, create one thats closest to your taste and customize it from there. 

  [Rouge]: https://github.com/rouge-ruby/rouge "Rouge on GitHub"

```bash
rougify style monokai > assets/css/syntax.css
```

**Tip:** if it doesn't already, make sure that your html links to your stylesheet in your html templates.
{: .notice--warning}

[Source](https://mcpride.github.io/posts/development/2018/03/06/syntax-highlighting-with-jekyll)

## Inline Highlighting

Unfortunately, Jekyll doesn't offer highlighting for inline code though and renders snippets like `this` without coloring.  This **CAN** be achieved though with a simple hack. One caveat however is that you *must* use the liquid way of highlights inline as opposed to the simpler way of wrapping with back-ticks.

First create the plugin `jekyll_inline_highlight.rb` in the `_plugins` directory of your project. Next copy the code found [here](https://github.com/bdesham/inline_highlight/raw/master/lib/jekyll_inline_highlight.rb). Finally wrap the text you'd like to highlight with a liquid tag. 

```liquid
{% raw %}{% ihighlight lang %} code {% endihighlight %}{% endraw %}
```

Note the '**i**' before as opposed to the standard `highlight` tag.
{: .notice--danger}

To verify this is working here's an example of highlighted python code {% ihighlight python %}printf("Hello.");{% endihighlight %} in a sentence.

Run `jekyll serve` to verify that your plugin works and renders your content and you're good to go! How easy was that?

[Source](https://www.bytedude.com/jekyll-syntax-highlighting-and-line-numbers/)
