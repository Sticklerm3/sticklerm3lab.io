---
title: Markdown Syntax Guide
category: General
tags:
  - markdown
  - jekyll
header:
  teaser: /assets/images/unsplash-writing.jpg
  overlay_image: /assets/images/overlay-macbook.jpg
  overlay_filter: 0.5
toc: true
toc_label: On this page
---
This is a demo of all styled elements in Jekyll now.

This is a paragraph, it's surrounded by whitespace. Next up are some headers, they're heavily influenced by GitHub's markdown style.

## Header 2 (H1 is reserved for post titles)

### Header 3

#### Header 4

##### Header 5

###### Header 6

## Links and Images

A link to [Jekyll Now](http://github.com/sticklerm3/). A big ass literal link <http://github.com/sticklerm3>

An image, located within /images

![an image alt text]( /assets/images/unsplash-writing.jpg "an image title")

## Lists

* A bulletted list
- alternative syntax 1
+ alternative syntax 2
  - an indented list item

1. An
2. ordered
3. list

## Emphasis 

Inline markup styles:

- _italics_
- **bold**
- `code()`

> Blockquote
>> Nested Blockquote

Syntax highlighting can be used with triple backticks, like so:

```javascript
/* Some pointless Javascript */
var rawr = ["r", "a", "w", "r"];
```

Use two trailing spaces  
on the right  
