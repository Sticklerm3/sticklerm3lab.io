---
title: Font Awesome upgraded to v5.13.0
description: New icons and more provided by Font Awesome!
category: General
tags: 
  - fontawesome
  - jekyll
date: 2020-03-30 10:24 -0500
header:
  overlay_image: /assets/images/teaser-macbook.jpg
  teaser: /assets/images/overlay-macbook.jpg
---
I've upgraded [Font Awesome] to version 5! This brings some new icons (including one of faves, the Old Republic logo) and some changes to how the API works. One of the most major changes is the deprecation of the `fa` prefix. This changes to `fas` for using solid icons and `fab` for brand icons. 

[Font Awesome]: https://fontawesome.com/

## Changes to the site code

This is mostly just here for my own reference in the future for things like future upgrades, adding new or additional fonts, etc. Font Awesome provides a guide for upgrading from v4 which is located [here](https://fontawesome.com/how-to-use/on-the-web/setup/upgrading-from-version-4). 

The theme I'm currently using has its templates all bundled into the root so that made it pretty simple. First, I followed the instructions provided by FA of deleting the old font files located in `assets/fonts` and adding the web fonts provided by their downloadable kit. Then I removed the CSS located at `assets/css/font-awesome.min.css`, replacing it with the `all.css` provided in the kit. Finally, all that's left is changing the path to the style-sheet in `_includes/head.html` and any fa references used.  Due to the fact that this site hasn't had much content added to it, the process was pretty simple by only having to change the icon preferences used at `index.html` and the `_includes/topnav.html`. 

To test everything, I ran `bundle exec jekyll serve` to preview and uh oh! The icons weren't rendering! A quick check of the run log had the following output:

```
[2020-03-30 09:43:29] ERROR `/assets/webfonts/fa-brands-400.woff2' not found.
[2020-03-30 09:43:29] ERROR `/assets/webfonts/fa-solid-900.woff2' not found.
[2020-03-30 09:43:29] ERROR `/assets/webfonts/fa-brands-400.woff' not found.
[2020-03-30 09:43:29] ERROR `/assets/webfonts/fa-solid-900.woff' not found.
[2020-03-30 09:43:29] ERROR `/assets/webfonts/fa-brands-400.ttf' not found.
[2020-03-30 09:43:29] ERROR `/assets/webfonts/fa-solid-900.ttf' not found.
```

Easy fix! All that needed to be done was to rename the directory the web-fonts are located in to `webfonts`. Quick refresh of the page in my browser and bam! Looking good! Now thats left to do is to make a new cheat sheet!
