---
title: Creating posts with jekyll-compose
description: This post was created with the `bundle exec jekyll draft` command
category:
  - Technology
tags:
  - jekyll
date: 2020-03-16 20:43 -0500
header:
  overlay_image: /assets/images/teaser-macbook.jpg
  teaser: /assets/images/overlay-macbook.jpg
---
## About and Installation

Quick guide and reference page to installing, configuring and using [jekyll-compose].

This post was created with the [jekyll-compose] plugin! 

[jekyll-compose]: https://github.com/jekyll/jekyll-compose

Install by adding `gem "jekyll-compose"` to the `group: jekyll_plugins` in your `Gemfile` and run `bundle install`.

## Usage

Create posts with the `draft` or `post` commands.  For example this post was created by running `bundle exec jekyll draft "Draft by compose"`. 

Publish drafts with `bundle exec jekyll publish _drafts/draft-by-compose.md`. 

To see a list of all available commands type `bundle exec jekyll` to get a list of all available commands.

## Configuring

You can configure `jekyll-compose` by adding a `jekyll_compose` section to your config file.

```yaml
jekyll_config:
  auto_open: true
```

When using the `auto_open` element jekyll will first check for the `JEKYLL_EDITOR` variable. then default to `EDITOR` or `VISUAL` with `VISUAL` taking precedence over the `EDITOR`. To configure `JEKYLL_EDITOR` add the following to your shells config file (~/.bashrc, ~/.zshrc, etc):

```bash
export JEKYLL_EDITOR=atom
```

You can also set the default frontmatter to use when creating a post in your `_config.yml` as well

```yaml
jekyll_compose:
  default_front_matter:
    drafts:
      description:
      image:
      category:
      tags:
    posts:
      description:
      image:
      category:
      tags:
      published: false
      sitemap: false
```

You can find more information about how to use and configure `jekyll-compose` [on their repo][jekyll-compose].
