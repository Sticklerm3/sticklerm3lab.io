---
title: Docs
author_profile: false
layout: collection
permalink: /docs/
collection: docs
sidebar:
  nav: "docs"
---
**Notice:** These pages are no longer relevant to this site.  They are merely still here for reference and to serve as practice when configuring the sidebar with *minimal-mistakes-jekyll*. **They will be removed in the near future**
{: .notice--danger}
