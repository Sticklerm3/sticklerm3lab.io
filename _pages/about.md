---
title: About Me
permalink: /about/
layout: single
tagline: Matt. Fucking. Stickler.
header:
  overlay_image: /assets/images/unsplash-star-wars.jpg
  overlay_filter: 0.5
  show_overlay_excerpt: true
---
Matthew Stickler.  
Born in Chicago, IL.  
I enjoy learning to code as a hobby. I'm a *huge* fan of Star Wars and lots of other geeky shit. I'm addicted to trance music, check out my latest scrobbles below!

{% include lastfm-widget.html %}

## About this site

This website is built with [Jekyll](https://jekyllrb.com) using the [minimal-mistakes](https://github.com/mmistakes/minimal-mistakes) theme and hosted using [GitLab pages](https://docs.gitlab.com/ee/user/project/pages/).
