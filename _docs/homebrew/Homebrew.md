---
title: Homebrew basics
permalink: /docs/homebrew/
---

[Homebrew] is a package manager that can download developer packages, applications, fonts, screensavers and more! To find out more, go to the [Homebrew homepage][Homebrew].

## Requirements

Make sure you have the XCode CLI tools installed:\*\*

```sh
$ xcode-select --install

# Accept user agreement
$ sudo xcodebuild -license accept
```

## Installation

```bash
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
```

## Casks

**Install casks with**

```sh
$ brew cask install <formula/token>
```

**Update with**

```sh
$ brew update
$ brew cask upgrade —-greedy
```

Search for casks simply using `brew search` or `brew search —-desc`. _The use of `brew cask search` has been deprecated_. You can uninstall formulas and casks hosted by third parties with the `brew tap` command. It uses a similar syntax to homebrew in that you call taps by `user/repository`

```sh
$ brew tap batman/homebrew-tap
$ brew cask install batman/tap/Gotham-maps
```

If a tap is hosted elsewhere than GitHub, then you will need to specify a URL after the `username/repository`. Tapping just uses `git clone` to download the tap, so any form accepted by git will work. For example, if a tap is hosted on bitbucket and is a private repo, use the `ssh` syntax of a repo after my tap name.

```sh
$ brew tap sticklerm3/homebrew-tap git@bitbucket.org:homebrew-tap.git
```

## External commands

### `brew bundle`

The `brew bundle` command is basically the same as the `ruby bundle` one except that it gathers all your formula, casks, and even the Mac App Store apps (using `mas`) so you can have an easy way to reinstall your apps & formula if need be, use it as a backup list, or so on.

```sh
# creates a global Brewfile for the system at $HOME/.Brewfile
$ brew bundle dump --global

# Checks installed formula to your global brewfile and informs you what will be (un)/installed before it does so
$ brew bundle cleanup --global --dryrun
```

## Links

-   [Homepage][Homebrew]
    -   [Documentation][brew-docs]
    -   [Homebrew Blog][brew-blog]

### On GitHub

-   [Homebrew][brew-github]
    -   [Core][brew-core]
    -   [Bundle][brew-bundle]
    -   [Services][brew-services]
    -   [Cask][brew-cask]
    -   [Cask-Fonts][cask-fonts]
    -   [Cask-Drivers][cask-drivers]
    -   [Command Not Found][brew-cnf]
    -   [Aliases][brew-alias]

### Other

-   [Homebrew Cask docs][cask-docs]
-   [(un)installer][brew-uninstall]

[Homebrew]: https://brew.sh "Homewbrew - Homepage"

[brew-docs]: https://docs.brew.sh/ "Homebrew - Docs"

[brew-blog]: https://brew.sh/blog/ "Homebrew - Blog"

[cask-docs]: https://github.com/Homebrew/homebrew-cask/tree/master/doc "Cask Docs"

[brew-bundle]: https://github.com/Homebrew/homebrew-bundle "brew bundle"

[brew-services]: https://github.com/Homebrew/homebrew-services "brew services"

[brew-core]: https://github.com/Homebrew/homebrew-core "Core on gh"

[brew-cask]: https://github.com/Homebrew/homebrew-cask "Cask-gh"

[cask-fonts]: https://github.com/Homebrew/homebrew-cask-fonts "Homebrew/cask-fonts"

[cask-drivers]: https://github.com/Homebrew/homebrew-cask-drivers "Homebrew/cask-drivers"

[brew-github]: https://github.com/Homebrew "Homebrew on GitHub"

[brew-uninstall]: https://github.com/Homebrew/install "un/installer"

[brew-cnf]: https://github.com/Homebrew/homebrew-command-not-found "CommandNotFound"

[brew-alias]: https://github.com/Homebrew/homebrew-aliases "brew-alias"
