---
title: Customizing jekyll-doc-theme
permalink: /docs/customization/
---
**Notice:** These pages are no longer relevant to this site.  They are merely still here for reference and to serve as practice when configuring the sidebar with *minimal-mistakes-jekyll*. **They will be removed in the near future**
{: .notice--danger}

This template uses [bootstrap-sass](https://github.com/twbs/bootstrap-sass) along with [bootwatch themes](https://bootswatch.com/3).
You can create your own theme by writing your own `sass` files.

Create a new a theme folder like `_sass/bootwatch/custom` and set your `bootwatch` variables in `_config.yml` to `custom`:

```yaml
bootwatch: custom
```
