---
title: Jekyll doc theme notes
permalink: /docs/jekyll-doc-theme/
---
**Notice:** These pages are no longer relevant to this site.  They are merely still here for reference and to serve as practice when configuring the sidebar with *minimal-mistakes-jekyll*. **They will be removed in the near future**
{: .notice--danger}

This websites theme and content is based on jekyll-doc-theme which can be found [here](https://github.com/aksakalli/jekyll-doc-theme). The information on this page has been copied from the original source to serve as a reference while I change the original material. 

## Writing content

### Docs

Docs are [collections](https://jekyllrb.com/docs/collections/) of pages stored under `_docs` folder. To create a new page:

**1.** Create a new Markdown as `_docs/my-page.md` and write [front matter](https://jekyllrb.com/docs/frontmatter/) & content such as:

```md
---
title: My Page
permalink: /docs/my-page/
---

Hello World!
```

**2.** Add the pagename to `_data/docs.yml` file in order to list in docs navigation panel:

```yml
- title: My Group Title
  docs:
  - my-page
```

### Blog posts

Add a new Markdown file such as `2017-05-09-my-post.md` and write the content similar to other post examples.

### Pages

The homepage is located under `index.html` file. You can change the content or design completely different welcome page for your taste. (You can use [bootstrap components](http://getbootstrap.com/components/))

In order to add a new page, create a new `.html` or `.md` (markdown) file under root directory and link it in `_includes/topnav.html`.
